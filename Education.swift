//
//  Education.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import Foundation
import CoreData


class Education: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    convenience init(nameInst:String,major:String,dataFrom:NSDate,dataTo:NSDate) {
        self.init(entity: NSEntityDescription.entityForName("Education", inManagedObjectContext: DataController.moc)!, insertIntoManagedObjectContext: DataController.moc)
        self.instName = nameInst
        self.major = major
        self.from = dataFrom
        self.to = dataTo
    }

}

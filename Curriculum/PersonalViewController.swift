//
//  PersonalViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/21/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class PersonalViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate  {
    
    var resume:Resume?
    var personal = PersonalView()
    var scrollView:UIScrollView!
    var currentKeyboardSize:CGFloat!
    //QUEUES
    let loadDataQueue = dispatch_queue_create("loadData", DISPATCH_QUEUE_CONCURRENT)
    let saveDataQueue = dispatch_queue_create("saveData", DISPATCH_QUEUE_CONCURRENT)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PersonalViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PersonalViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PersonalViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    
        scrollView = UIScrollView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        scrollView.contentSize = CGSize(width: UIScreen.mainScreen().bounds.width, height: personal.frame.height)
        
        self.loadData()
        
        self.navigationItem.title = NSLocalizedString("personal", comment: "personal")
        scrollView.addSubview(personal)
        self.view.addSubview(scrollView)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.personal.imageProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PersonalViewController.imageProfileTapped(_:))))
        
    }
    
    //MARK: Image Chooser
    
    func imageProfileTapped(img: AnyObject) {
        let alert = UIAlertController(title: NSLocalizedString("choose", comment: "choose"), message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("gallery", comment: "gallery"), style: UIAlertActionStyle.Default, handler: {
            (alertAction) -> Void in self.pickFromGallery()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("takePic", comment: "takePic"), style: UIAlertActionStyle.Default, handler: {
            (alertAction) -> Void in self.takeAPicture()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .Destructive, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func pickFromGallery() {
        self.personal.wasImageChanged = true
        let pickerController = UIImagePickerController()
        pickerController.sourceType = .PhotoLibrary
        pickerController.delegate = self
        presentViewController(pickerController, animated: true, completion: nil)
    }
    
    func takeAPicture() {
        self.personal.wasImageChanged = true
        let pickerController = UIImagePickerController()
        pickerController.sourceType = .Camera
        pickerController.delegate = self
        presentViewController(pickerController, animated: true, completion: nil)
    }
    
        
    // MARK: ImagePickerDelagate
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
        self.personal.wasImageChanged = false
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        var selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        selectedImage = ImageManager.cropAndResizeImage(selectedImage, newSide: 250)
        self.personal.imageProfile.image = selectedImage
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    //MARK: keyboard handling
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            if self.scrollView.contentSize.height <= self.personal.frame.height {
                self.scrollView.contentSize.height += keyboardSize.height
                self.currentKeyboardSize = keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if self.scrollView.contentSize.height > self.personal.frame.height{
            self.scrollView.contentSize.height -= self.currentKeyboardSize
        }
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        self.saveData()
        //SETTING THE RESUME TO THE NEXT VIEW
        let dest = segue.destinationViewController as! EducationViewController
        dest.resume = self.resume
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        let res =  self.personal.validateAllFields()
        if res {
            return res
        } else {
            let alert = UIAlertController(title: NSLocalizedString("alert", comment: "alert"), message: NSLocalizedString("personalNotValidated", comment: "notValidated"), preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
            return res
        }
    }
    
    // MARK: Concurrent tasks
    
    func loadData() {
        dispatch_async(self.loadDataQueue) {
            if self.resume?.personal != nil {
                self.personal.loadData(self.resume!)
            }
        }
    }
    
    func saveData() {
        dispatch_async(self.saveDataQueue) {
            //SAVING PERSONAL INFORMATION
            let personal = self.personal.saveFields()
            self.resume?.personal = personal
            if let imageData = self.personal.getChosenImage() {
                let imageResume = ImageResume()
                imageResume.image = imageData
                self.resume?.image = imageResume
            }
            //create owner by taking the first and last name if exists
            let arrName = self.personal.fieldName.text?.componentsSeparatedByString(" ")
            var name = arrName![0]
            if arrName!.count > 1 {
                name = "\(arrName![0])_\(arrName![arrName!.count-1])"
            }
            self.resume?.owner = name
            
            do {
                try DataController.moc.save()
            } catch {
                print("Error during saving personal information \(error)")
            }
            PDFManager.deletePDF(self.resume!)
        }
    }
}

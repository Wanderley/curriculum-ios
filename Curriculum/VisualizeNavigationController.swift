//
//  VisualizeNavigationController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/27/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class VisualizeNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barStyle = UIBarStyle.Black
        self.navigationBar.tintColor = UIColor.whiteColor()
    }

}

//
//  PDFVisualizerController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/28/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class PDFVisualizerController: UIViewController {

    var address: String?
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        do {
            let url : NSURL! = try NSURL(string: self.address!)
            self.webView.loadRequest(NSURLRequest(URL: url))
            self.navigationItem.title = "PDF"
        } catch {
            fatalError("Error when loading PDF \(error)")
        }
    }
}

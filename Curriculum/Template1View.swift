//
//  Template1View.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/28/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class Template1View: UIView {
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var labelHeaderContact: UILabel!
    @IBOutlet weak var labelFacebook: UILabel!
    @IBOutlet weak var labelLinkedin: UILabel!
    @IBOutlet weak var labelEducation: UILabel!
    @IBOutlet weak var labelEducationContent: UILabel!
    @IBOutlet weak var labelExperience: UILabel!
    @IBOutlet weak var labelExperienceContent: UILabel!
    @IBOutlet weak var labelTech: UILabel!
    @IBOutlet weak var labelTechContent: UILabel!
    @IBOutlet weak var labelAdditionalInfo: UILabel!
    @IBOutlet weak var labelAdditionalInfoContent: UILabel!
    
    init() {
        super.init(frame: CGRectMake(0, 0, 0, 0))
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    //MARK: setupView
    func setup() {
        let xib = NSBundle.mainBundle().loadNibNamed("Template1", owner: self, options: nil)[0] as! Template1View
        xib.frame = CGRectMake(0, 0, 612, 792)
        self.frame = xib.frame
        self.addSubview(xib)
    }

}

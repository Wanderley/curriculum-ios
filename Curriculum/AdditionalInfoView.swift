//
//  AdditionalInfoView.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class AdditionalInfoView: UIView, UITextViewDelegate {

    @IBOutlet weak var textAddInfo: UITextView!
   
    init() {
        super.init(frame: CGRectMake(0, 0, 0, 0))
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        let xib = NSBundle.mainBundle().loadNibNamed("AdditionalInfoView", owner: self, options: nil)[0] as! AdditionalInfoView
        xib.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width - 10, 320)
        
        self.textAddInfo.delegate = self
        self.textAddInfo.layer.borderWidth = 0.5
        self.textAddInfo.layer.cornerRadius = 5
        self.textAddInfo.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.textAddInfo.text = NSLocalizedString("addInfoPlaceH", comment: "")
        
        self.frame = xib.frame
        self.addSubview(xib)
    }
    
    //MARK: save fields
    
    func saveFields() -> AdditionalInfo?{
        if self.textAddInfo.text != NSLocalizedString("addInfoPlaceH", comment: "") && self.textAddInfo.text != "" {
            return AdditionalInfo(addInfo: self.textAddInfo.text!)
        } else {
            return nil
        }
    }
    
    //MARK: textview delegate
    
    func textViewDidBeginEditing(textView: UITextView) {
        if self.textAddInfo.text == NSLocalizedString("addInfoPlaceH", comment: "") {
            self.textAddInfo.text = ""
        }
    }
    
    func textViewDidChange(textView: UITextView) {
        if self.textAddInfo.text == "" {
            self.textAddInfo.text = NSLocalizedString("addInfoPlaceH", comment: "")
        } else if self.textAddInfo.text.containsString(NSLocalizedString("addInfoPlaceH", comment: "")) {
            self.textAddInfo.text = ""
        }
    }

}

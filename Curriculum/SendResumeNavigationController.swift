//
//  SendResumeNavigationController.swift
//  Curriculum Manager
//
//  Created by Wanderley Filho on 4/4/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class SendResumeNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barStyle = UIBarStyle.Black
        self.navigationBar.tintColor = UIColor.whiteColor()
    }
    
}

//
//  FirstViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/20/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit
import CoreData

class ResumesViewController: UITableViewController {
    
    var resumes:[Resume]?
    let loadQueue = dispatch_queue_create("loadImages", DISPATCH_QUEUE_CONCURRENT)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "CurriculumCell", bundle: nil), forCellReuseIdentifier: "CurriculumCell")
        navigationItem.leftBarButtonItem = editButtonItem()
        
        let triggerTime = (Int64(NSEC_PER_SEC) * 1)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue()) {
            if self.resumes?.count == 0 {
                self.loadResumes()
                self.tableView.reloadData()
            }

        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.loadResumes()
        self.tableView.reloadData()
    }
    
    func loadResumes() {
        do {
            self.resumes = try DataController.moc.executeFetchRequest(NSFetchRequest(entityName: "Resume")) as! [Resume]
        } catch {
            print("Message error :\(error)")
        }
    }
    
    //MARK: tableView
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.resumes?.count)!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CurriculumCell", forIndexPath: indexPath) as! CurriculumTableViewCell
        let resume = self.resumes![indexPath.row]
        cell.labelName.text = resume.owner?.stringByReplacingOccurrencesOfString("_", withString: " ")
        cell.imageResume.layer.cornerRadius = 3
        
        if resume.owner == NSLocalizedString("notDefined", comment: "") {
            cell.labelDetails.text = NSLocalizedString("notDefined", comment: "")
        } else {
            self.loadImageInBackGround(resume.image, imagePosition: cell.imageResume)
            
            let age = resume.personal?.age
            let email = resume.personal?.email
            cell.labelDetails.text = "\(age!), \(email!)"
            
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("editResume", sender: indexPath)
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            PDFManager.deletePDF(self.resumes![indexPath.row])
            DataController.moc.deleteObject(self.resumes![indexPath.row])
            do {
                try DataController.moc.save()
            } catch {
                
            }
            self.resumes?.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }

    //MARK: navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let dest = segue.destinationViewController as! PersonalViewController
        if segue.identifier == "newResume" {
            let newResume = NSEntityDescription.insertNewObjectForEntityForName("Resume", inManagedObjectContext: DataController.moc) as! Resume
            newResume.owner = NSLocalizedString("notDefined", comment: "")
            newResume.lastPdfName = ""
            do {
                try DataController.moc.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            dest.resume = newResume
            
        } else if segue.identifier == "editResume" {
            let index = sender as! NSIndexPath
            dest.resume = self.resumes![index.row]
        }
    }
    
    //MARK: concurrent tasks
    func loadImageInBackGround(imageResume:ImageResume?, imagePosition: UIImageView) {
        
        dispatch_async(self.loadQueue) {
            if let data = imageResume?.image {
                let image = UIImage(data: data)
                dispatch_async(dispatch_get_main_queue()) {
                    imagePosition.image = image
                }
            }
        }
    }
}


//
//  InstructionsViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 4/3/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class InstructionsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("instructions", comment: "")
        
        let instructionsView = InstructionsView()
        
        let scroll = UIScrollView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        scroll.contentSize = CGSize(width: instructionsView.frame.width, height: instructionsView.frame.height + 40)
        scroll.delaysContentTouches = false
        scroll.addSubview(instructionsView)
        
        self.view.addSubview(scroll)

    }

}

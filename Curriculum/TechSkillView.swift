//
//  TechSkillView.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class TechSkillView: UIView, UITextViewDelegate {

    @IBOutlet weak var textSkills: UITextView!
    
    init() {
        super.init(frame: CGRectMake(0, 0, 0, 0))
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setup() {
        let xib = NSBundle.mainBundle().loadNibNamed("TechSkillView", owner: self, options: nil)[0] as! TechSkillView
        xib.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width - 10, 320)
        
        self.textSkills.delegate = self
        self.textSkills.layer.borderWidth = 0.5
        self.textSkills.layer.cornerRadius = 5
        self.textSkills.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.textSkills.text = NSLocalizedString("techSkillsPlaceH", comment: "")
        
        self.frame = xib.frame
        self.addSubview(xib)
    }
    
    //MARK: save fields
    
    func saveFields() -> Skills?{
        if self.textSkills.text != NSLocalizedString("techSkillsPlaceH", comment: "") && self.textSkills.text != "" {
            return Skills(skills: self.textSkills.text!)
        } else {
            return nil
        }
    }
    
    //MARK: textview delegate
    
    func textViewDidBeginEditing(textView: UITextView) {
        if self.textSkills.text == NSLocalizedString("techSkillsPlaceH", comment: "") {
            self.textSkills.text = ""
        }
    }
    
    func textViewDidChange(textView: UITextView) {
        if self.textSkills.text == "" {
            self.textSkills.text = NSLocalizedString("techSkillsPlaceH", comment: "")
        } else if self.textSkills.text.containsString(NSLocalizedString("techSkillsPlaceH", comment: "")) {
            self.textSkills.text = ""
        }
    }

}

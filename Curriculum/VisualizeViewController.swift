//
//  SecondViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/20/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit
import CoreData

class VisualizeViewController: UITableViewController {

    var resumes:[Resume]?
    let loadQueue = dispatch_queue_create("loadImages", DISPATCH_QUEUE_CONCURRENT)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("viewPdf", comment: "")
        tableView.registerNib(UINib(nibName: "VisualizeCurriculumCell", bundle: nil), forCellReuseIdentifier: "VisualizeCell")
    }
    
    override func viewWillAppear(animated: Bool) {
        self.loadResumes()
        self.tableView.reloadData()
    }
    
    func loadResumes() {
        do {
            self.resumes = try DataController.moc.executeFetchRequest(NSFetchRequest(entityName: "Resume")) as! [Resume]
        } catch {
            fatalError("Message error :\(error)")
        }
    }
    
    //MARK: tableView
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return HeaderView()
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.resumes?.count)!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("VisualizeCell", forIndexPath: indexPath) as! VisualizeCurriculumTableViewCell
        let resume = self.resumes![indexPath.row]
        cell.labelName.text = resume.owner?.stringByReplacingOccurrencesOfString("_", withString: " ")
        cell.changeStatusView(resume)
        cell.imageResume.layer.cornerRadius = 3
        
        if resume.owner == NSLocalizedString("notDefined", comment: "") {
            cell.labelDetails.text = NSLocalizedString("notDefined", comment: "")
        } else {
            self.loadImageInBackGround(resume.image, imagePosition: cell.imageResume)
            let age = resume.personal?.age
            let email = resume.personal?.email
            cell.labelDetails.text = "\(age!), \(email!)"
            
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        shouldPerformSegueWithIdentifier("showPdf", sender: indexPath)
    }
    
    //MARK: segue
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        let index = sender as! NSIndexPath
        let resume = self.resumes![index.row]
        if resume.owner != NSLocalizedString("notDefined", comment: "") {
            performSegueWithIdentifier("showPdf", sender: index)
            return true
        } else {
            let alert = UIAlertController(title: NSLocalizedString("alert", comment: "alert"), message: NSLocalizedString("personalNotValidated", comment: "notValidated"), preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
            return false
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let index = sender as! NSIndexPath
        let resume = self.resumes![index.row]
        
        let address = PDFManager.generatePdf(resume)
        
        let dest = segue.destinationViewController as! PDFVisualizerController
        dest.address = address
    }
    
    //MARK: concurrent tasks
    func loadImageInBackGround(imageResume:ImageResume?, imagePosition: UIImageView) {
        
        dispatch_async(self.loadQueue) {
            if let data = imageResume?.image {
                let image = UIImage(data: data)
                dispatch_async(dispatch_get_main_queue()) {
                    imagePosition.image = image
                }
            }
        }
    }

}


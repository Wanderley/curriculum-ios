//
//  InstructionsView.swift
//  Curriculum
//
//  Created by Wanderley Filho on 4/3/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class InstructionsView: UIView {

    @IBOutlet weak var viewSteps: UIView!
    @IBOutlet weak var labelSteps: UILabel!
    @IBOutlet weak var buttonCreating: UIButton!
    @IBOutlet weak var buttonUpdating: UIButton!
    @IBOutlet weak var buttonSending: UIButton!
    @IBOutlet weak var buttonDeleting: UIButton!
    
    init() {
        super.init(frame: CGRectZero)
        setup()
        adaptLanguage()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        let xib = NSBundle.mainBundle().loadNibNamed("InstructionsView", owner: self, options: nil)[0] as! InstructionsView
        xib.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, xib.frame.height)
        
        self.viewSteps.layer.borderWidth = 1
        self.viewSteps.layer.cornerRadius = 8
        
        self.frame = xib.frame
        self.addSubview(xib)
    }

    func adaptLanguage() {
        self.buttonCreating.setTitle(NSLocalizedString("creatingR", comment: ""), forState: .Normal)
        self.buttonUpdating.setTitle(NSLocalizedString("updatingR", comment: ""), forState: .Normal)
        self.buttonSending.setTitle(NSLocalizedString("sendingR", comment: ""), forState: .Normal)
        self.buttonDeleting.setTitle(NSLocalizedString("deletingR", comment: ""), forState: .Normal)
        self.labelSteps.text = NSLocalizedString("selectTopic", comment: "")
    }
    
    @IBAction func creatingTapped(sender: AnyObject) {
        self.labelSteps.text = NSLocalizedString("aboutInstCreating", comment: "")
    }
    
    @IBAction func updatingTapped(sender: AnyObject) {
        self.labelSteps.text = NSLocalizedString("aboutInstUpdating", comment: "")
    }
    
    @IBAction func sendingTapped(sender: AnyObject) {
        self.labelSteps.text = NSLocalizedString("aboutInstSending", comment: "")
    }
    
    @IBAction func deletingTapped(sender: AnyObject) {
        self.labelSteps.text = NSLocalizedString("aboutInstDeleting", comment: "")
    }
}

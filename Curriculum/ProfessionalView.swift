//
//  ProfessionalView.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class ProfessionalView: UIView, UITextViewDelegate {

    @IBOutlet weak var labelOrganization: UILabel!
    @IBOutlet weak var fieldOrganization: UITextField!
    @IBOutlet weak var labelRole: UILabel!
    @IBOutlet weak var fieldRole: UITextField!
    @IBOutlet weak var labelJob: UILabel!
    @IBOutlet weak var textJob: UITextView!
    @IBOutlet weak var labelCurrent: UILabel!
    @IBOutlet weak var switchCurrent: UISwitch!
    @IBOutlet weak var labelFrom: UILabel!
    @IBOutlet weak var dateFrom: UIDatePicker!
    @IBOutlet weak var labelTo: UILabel!
    @IBOutlet weak var dateTo: UIDatePicker!
    
    init() {
        super.init(frame: CGRectMake(0, 0, 0, 0))
        setup()
        adaptLanguage()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: setup
    
    func setup() {
        let xib = NSBundle.mainBundle().loadNibNamed("ProfessionalView", owner: self, options: nil)[0] as! ProfessionalView
        
        self.textJob.delegate = self
        self.textJob.layer.borderWidth = 0.5
        self.textJob.layer.cornerRadius = 5
        self.textJob.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        self.switchCurrent.addTarget(self, action: #selector(ProfessionalView.changeCurrent), forControlEvents: .ValueChanged)
        
        xib.layer.borderWidth = 1
        xib.layer.cornerRadius = 8
        
        xib.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width - 10, 700)
        self.frame = xib.frame
        self.addSubview(xib)

    }
    
    func changeCurrent() {
        if self.switchCurrent.on {
            self.labelTo.hidden = true
            self.dateTo.hidden = true
        } else {
            self.labelTo.hidden = false
            self.dateTo.hidden = false
        }
    }
    
    //MARK: adapt language
    
    func adaptLanguage() {
        self.labelOrganization.text = "\(NSLocalizedString("organizationName", comment: "")):"
        self.labelRole.text = "\(NSLocalizedString("role", comment: "")):"
        self.labelJob.text = "\(NSLocalizedString("jobDesc", comment: "")):"
        self.textJob.text = NSLocalizedString("jobDescPlaceH", comment: "")
        self.labelCurrent.text = NSLocalizedString("currentJob", comment: "")
        self.labelFrom.text = "\(NSLocalizedString("from", comment: "")):"
        self.labelTo.text = "\(NSLocalizedString("to", comment: "")):"
    }
    
    func saveFields() -> Experience? {
        if self.fieldOrganization.text != "" && self.fieldRole.text != "" {
            let exp = Experience(organization: self.fieldOrganization.text!, role: self.fieldRole.text!, isCurrent: self.switchCurrent.on, from: self.dateFrom.date)
            if self.textJob.text != NSLocalizedString("jobDescPlaceH", comment: "") {
                exp.jobDesc = self.textJob.text
            } else {
                exp.jobDesc = ""
            }
            if self.switchCurrent.on {
                exp.to = NSDate()
            } else {
                exp.to = self.dateTo.date
            }
            return exp
        } else {
            return nil
        }
    }
    
    func loadData(exp:Experience) {
        self.fieldOrganization.text = exp.orgName
        self.fieldRole.text = exp.role
        self.switchCurrent.setOn((exp.isCurrent?.boolValue)!, animated: true)
        self.changeCurrent()
        self.textJob.text = exp.jobDesc
        self.dateFrom.setDate(exp.from!, animated: true)
        self.dateTo.setDate(exp.to!, animated: true)
    }
    
    //MARK: textview delegate
    
    func textViewDidBeginEditing(textView: UITextView) {
        if self.textJob.text == NSLocalizedString("jobDescPlaceH", comment: "") {
            self.textJob.text = ""
        }
    }
    func textViewDidChange(textView: UITextView) {
        if self.textJob.text == "" {
            self.textJob.text = NSLocalizedString("jobDescPlaceH", comment: "")
        } else if self.textJob.text.containsString(NSLocalizedString("jobDescPlaceH", comment: "")) {
            self.textJob.text = ""
        }
    }
}

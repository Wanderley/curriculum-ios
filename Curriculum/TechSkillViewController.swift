//
//  TechSkillsViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class TechSkillViewController: UIViewController {

    var resume:Resume?
    var techView: TechSkillView!
    var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //work around to make the title selectable
        let titleView = UILabel()
        titleView.text = NSLocalizedString("techSkills", comment: "")
        titleView.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        titleView.textColor = UIColor.whiteColor()
        let width = titleView.sizeThatFits(CGSizeMake(CGFloat.max, CGFloat.max)).width
        titleView.frame = CGRect(origin:CGPointZero, size:CGSizeMake(width, 50))
        self.navigationItem.titleView = titleView
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TechSkillViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.navigationItem.titleView = titleView
        self.navigationItem.titleView?.userInteractionEnabled = true
        self.navigationItem.titleView?.addGestureRecognizer(tap)
        
        self.techView = TechSkillView()
        self.techView?.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 320)
        
        self.scrollView = UIScrollView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        self.scrollView.contentSize = CGSize(width: UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height)
        
        self.loadSkillsData()
        
        self.scrollView.addSubview(self.techView)
        self.view.addSubview(self.scrollView)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func loadSkillsData() {
        if let skill = self.resume?.skills {
            self.techView.textSkills.text = skill.skillsDesc
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //SAVING
        let tech = self.techView.saveFields()
        self.resume?.skills = tech
        do {
            try DataController.moc.save()
        } catch {
            fatalError("Error during saving in Skills: \(error)")
        }
        //SETTING RESUME IN ADDITIONAL INFORMATION
        let dest =  segue.destinationViewController as! AdditionalInfoViewController
        dest.resume = self.resume
    }
}

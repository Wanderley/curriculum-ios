//
//  AdditionalInfoViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class AdditionalInfoViewController: UIViewController {

    var resume:Resume?
    var additionalInfoView: AdditionalInfoView!
    var scrollView:UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //work around to make the title selectable
        let titleView = UILabel()
        titleView.text = NSLocalizedString("additionalInfo", comment: "")
        titleView.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        titleView.textColor = UIColor.whiteColor()
        let width = titleView.sizeThatFits(CGSizeMake(CGFloat.max, CGFloat.max)).width
        titleView.frame = CGRect(origin:CGPointZero, size:CGSizeMake(width, 50))
        self.navigationItem.titleView = titleView
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AdditionalInfoViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.navigationItem.titleView = titleView
        self.navigationItem.titleView?.userInteractionEnabled = true
        self.navigationItem.titleView?.addGestureRecognizer(tap)
        
        self.additionalInfoView = AdditionalInfoView()
        self.additionalInfoView?.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 320)
        
        self.scrollView = UIScrollView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        self.scrollView.contentSize = CGSize(width: UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height)
        
        self.loadAddData()
        
        self.scrollView.addSubview(self.additionalInfoView)
        self.view.addSubview(self.scrollView)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func loadAddData() {
        if let add = self.resume?.additionalInfo {
            self.additionalInfoView.textAddInfo.text = add.information
        }
    }
    
    @IBAction func saveButtonTapped(sender: AnyObject) {
        let addInfo = self.additionalInfoView.saveFields()
        self.resume?.additionalInfo = addInfo
        
        do {
            try DataController.moc.save()
        } catch {
            fatalError("Error during saving in Additional Info: \(error)")
        }
        let alert = UIAlertController(title: NSLocalizedString("congrats", comment: "alert"), message: NSLocalizedString("messageCongrats", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: {
            (alertAction) -> Void in self.navigationController?.popToRootViewControllerAnimated(true)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

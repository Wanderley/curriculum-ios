//
//  PDFManager.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/30/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class PDFManager: NSObject {
    
    static func generatePdf(resume:Resume) -> String {
        let address = reCreatePDF(resume)
        
        let gen = PDFGeneratorT1()
        gen.resume = resume
        gen.drawPdf(address)
        
        return address
    }
 
    static func reCreatePDF(resume:Resume) -> String{
        let fileManager = NSFileManager.defaultManager()
        let documentDirectoryURLs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        var path = documentDirectoryURLs.first?.URLByAppendingPathComponent("\(resume.lastPdfName!).pdf").path
        
        if resume.lastPdfName != "" {
            do {
                try fileManager.removeItemAtPath(path!)
                
            } catch let error as NSError {
                print("ERROR: \(error)")
            }
        }
        
        path = documentDirectoryURLs.first?.URLByAppendingPathComponent("\(resume.owner!).pdf").path
        resume.lastPdfName = resume.owner!
        
        do {
            try DataController.moc.save()
        } catch {
            print("Error when was saving PDF File path: \(error)")
        }
        
        return path!
    }
    
    static func deletePDF(resume:Resume) {
        let fileManager = NSFileManager.defaultManager()
        let documentDirectoryURLs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        let path = documentDirectoryURLs.first?.URLByAppendingPathComponent("\(resume.lastPdfName!).pdf").path
        
        if fileManager.fileExistsAtPath(path!){
            
            do {
                try fileManager.removeItemAtPath(path!)
                
            } catch let error as NSError {
                print("ERROR: \(error)")
            }
        }
    }
    
    static func checkIfPdfExists(resume:Resume) -> Bool {
        let fileManager = NSFileManager.defaultManager()
        let documentDirectoryURLs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        let path = documentDirectoryURLs.first?.URLByAppendingPathComponent("\(resume.lastPdfName!).pdf").path
        
        return fileManager.fileExistsAtPath(path!)
    }
    
    static func getPDFPath(resume:Resume) -> String {
        let fileManager = NSFileManager.defaultManager()
        let documentDirectoryURLs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        let path =  documentDirectoryURLs.first?.URLByAppendingPathComponent("\(resume.lastPdfName!).pdf").path
        
        return path!
    }
}

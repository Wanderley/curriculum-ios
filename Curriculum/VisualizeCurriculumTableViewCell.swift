//
//  VisualizeCurriculumTableViewCell.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/30/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class VisualizeCurriculumTableViewCell: UITableViewCell {

    @IBOutlet weak var imageResume: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func changeStatusView(resume:Resume) {
        self.viewStatus.layer.borderWidth = 0.5
        self.viewStatus.layer.cornerRadius = 13
        
        if resume.owner == NSLocalizedString("notDefined", comment: "") {
            self.viewStatus.backgroundColor = UIColor.whiteColor()
            
        } else if resume.personal != nil && resume.education!.count > 0 && resume.experience!.count > 0 && resume.skills != nil && resume.additionalInfo != nil {
            self.viewStatus.backgroundColor = UIColor.darkGrayColor()
            
        } else {
            self.viewStatus.backgroundColor = UIColor.lightGrayColor()
        }
    }
}

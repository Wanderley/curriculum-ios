//
//  EducationViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/23/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class EducationViewController: UIViewController {
    
    var resume: Resume?
    var educationViews = [EducationView]()
    var addBtn:UIButton!
    var scrollView: UIScrollView!
    var limitCounter = 4;
    
    //QUEUES
    let loadEducationsQueue = dispatch_queue_create("loadEducations", DISPATCH_QUEUE_CONCURRENT)
    let saveEducationQueue = dispatch_queue_create("saveEducations", DISPATCH_QUEUE_CONCURRENT)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("education", comment: "")
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EducationViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        let ed = EducationView()
        ed.frame = CGRectMake(5, getBottomPosition(), UIScreen.mainScreen().bounds.width - 10, 600)
        self.educationViews.append(ed)
        self.scrollView = UIScrollView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        self.scrollView.contentSize = CGSize(width: UIScreen.mainScreen().bounds.width, height: educationViews[0].frame.height + 60)
        
        self.scrollView.addSubview(educationViews[0])
        
        self.addBtn = UIButton(type: UIButtonType.System) as UIButton
        self.addBtn.frame = CGRectMake(10,0,UIScreen.mainScreen().bounds.width-20,40)
        self.addBtn.titleLabel?.font = UIFont(name: (self.addBtn.titleLabel?.font?.fontName)!, size: 18)
        self.addBtn.setTitle("\(NSLocalizedString("addButton", comment: "")) \(NSLocalizedString("education", comment: ""))", forState: .Normal)
        self.addBtn.addTarget(self, action: #selector(EducationViewController.addNewEducation), forControlEvents: .TouchUpInside)
        self.addBtn.frame.origin.y = self.getBottomPosition()
        
        self.loadEducationData()
        
        scrollView.addSubview(self.addBtn)
        self.view.addSubview(scrollView)
    }
    
    //MARK: managing view
    func getBottomPosition() -> CGFloat{
        var posY:CGFloat = 5
        for _ in self.educationViews {
            posY += 605
        }
        return posY
    }
    
    func addNewEducation() {
        let ed = EducationView()
        ed.frame = CGRectMake(5, getBottomPosition(), UIScreen.mainScreen().bounds.width - 10, 600)
        self.educationViews.append(ed)
        self.scrollView.contentSize.height = CGFloat((self.educationViews.count * 605) + 60)
        self.scrollView.addSubview(ed)
        self.addBtn.frame.origin.y = self.getBottomPosition()
        self.limitCounter -= 1
        
        if self.limitCounter == 0 {
            self.addBtn.removeFromSuperview()
        }
    }
    
    //MARK: navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        self.saveEducationData()
        //SETTING RESUME
        let dest = segue.destinationViewController as! ProfessionalViewController
        dest.resume = self.resume
    }
    
    //MARK: keyboard handling
    
    func keyboardWillShow(notification: NSNotification) {
        for ed in educationViews {
            ed.dateFrom.userInteractionEnabled = false;
            ed.dateTo.userInteractionEnabled = false;
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        for ed in educationViews {
            ed.dateFrom.userInteractionEnabled = true;
            ed.dateTo.userInteractionEnabled = true;
        }
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: Concurrent Tasks
    func loadEducationData() {
        let educations = self.resume?.education?.allObjects as! [Education]
        let count =  educations.count
        if count > 0 {
            for _ in 0 ..< (count - 1) {
                self.addNewEducation()
            }
            dispatch_async(self.loadEducationsQueue) {
                for index in 0 ..< count {
                    let edView = self.educationViews[index]
                    let edData = educations[index]
                    dispatch_async(dispatch_get_main_queue()) {
                        edView.loadData(edData)
                    }
                }
            }
        }
    }
    
    func saveEducationData() {
        dispatch_async(self.saveEducationQueue) {
            let educations = NSMutableSet()
            for edView in self.educationViews {
                if let ed = edView.saveFields() {
                    educations.addObject(ed)
                }
            }
            self.resume?.education = educations
            do {
                try DataController.moc.save()
            } catch {
                fatalError("Error during saving educations: \(error)")
            }
        }
    }

}

//
//  PDFGenerator.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/28/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import CoreText
import UIKit

class PDFGeneratorT1: NSObject {
    
    var diff:CGFloat = 0
    var resume:Resume?
    var fontTitle = CTFontCreateWithName("HelveticaNeue-Bold" as CFStringRef, 19.0, nil)
    var fontNormal = CTFontCreateWithName("HelveticaNeue" as CFStringRef, 13.0, nil)
    var fontNormalBold = CTFontCreateWithName("HelveticaNeue-Bold" as CFStringRef, 13.0, nil)
    let templateView = Template1View()
    
    func drawPdf(filePath: String) {
        
        UIGraphicsBeginPDFContextToFile(filePath, CGRectZero,nil)
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
        
        drawPersonalLabels()
        let ed = self.resume?.education
        if ed?.count != 0 {
            drawEducationSection()
        }
        let exp = self.resume?.experience
        if exp?.count != 0 {
            drawProfessionalExperience()
        }
        let tech = self.resume?.skills
        if tech != nil {
            drawSkills()
        }
        let info = self.resume?.additionalInfo
        if info != nil {
            drawAdditionalInfo()
        }
        
        UIGraphicsEndPDFContext()

    }
    
    func drawPersonalLabels() {
        
        //draw name
        drawText((resume?.personal?.name)!, frameRect: templateView.labelName.frame, isTitle: true, isName: true)
        //drawPhoto
        if let imgData = resume!.image?.image {
            UIImage(data: imgData)?.drawInRect(templateView.imageProfile.frame)
        }
        //draw address information
        drawText(self.createHeaderString(), frameRect: templateView.labelHeader.frame, isTitle: false)
        //draw contact information
        drawText(self.createHeaderContactString(), frameRect: templateView.labelHeaderContact.frame, isTitle: false)
        //draw facebook and/or linkedin
        let personal = (self.resume!.personal)!
        if personal.facebook! != "" {
            drawText(personal.facebook!, frameRect: templateView.labelFacebook.frame, isTitle: false)
            drawText(personal.linkedin!, frameRect: templateView.labelLinkedin.frame, isTitle: false)
        } else if personal.linkedin! != "" {
            drawText(personal.linkedin!, frameRect: templateView.labelFacebook.frame, isTitle: false)
        }
    }
    
    func drawEducationSection() {
        let education = (self.resume?.education)! as NSSet
        //draw education title
        drawText(NSLocalizedString("education", comment: ""), frameRect: templateView.labelEducation.frame, isTitle: true)
        
        let textString = createEducationContentString(education)
        templateView.labelEducationContent.text = textString
        templateView.labelEducationContent.sizeToFit()
        
        let ranges = getEducationRanges(education, text: textString)
        drawTextWithBoldTitles(textString, frameRect: templateView.labelEducationContent.frame, ranges: ranges)
    }
    
    func drawProfessionalExperience() {
        let experience = (self.resume?.experience)! as NSSet
        
        var rect = templateView.labelExperience.frame
        let lastFrame = templateView.labelEducationContent.frame
        if checkSize(lastFrame.origin.y + lastFrame.size.height, heightContent: rect.height) {
            rect.origin.y = (lastFrame.origin.y + lastFrame.size.height) - self.diff
            self.diff = 0
        } else {
            rect.origin.y = (lastFrame.origin.y + lastFrame.size.height)
        }
        drawText(NSLocalizedString("prettyProfessionalExp", comment: ""), frameRect: rect, isTitle: true)
        
        let textString = createExperienceContentString(experience)
        
        templateView.labelExperienceContent.text = textString
        templateView.labelExperienceContent.sizeToFit()
        if checkSize(rect.origin.y + rect.size.height, heightContent: templateView.labelExperienceContent.frame.height) {
            templateView.labelExperienceContent.frame.origin.y = (rect.origin.y + rect.size.height) - self.diff
            self.diff = 0
        } else {
            templateView.labelExperienceContent.frame.origin.y = (rect.origin.y + rect.size.height)
        }
        let ranges = getExperienceRanges(experience, text: textString)
        drawTextWithBoldTitles(textString, frameRect: templateView.labelExperienceContent.frame, ranges: ranges)
    }
    
    func drawSkills() {
        let skill = (self.resume?.skills)!
        
        var rect = templateView.labelTech.frame
        let lastFrame = templateView.labelExperienceContent.frame
        if checkSize(lastFrame.origin.y + lastFrame.size.height,heightContent: rect.height) {
            rect.origin.y = (lastFrame.origin.y + lastFrame.size.height) - self.diff
            self.diff = 0
        } else {
            rect.origin.y = (lastFrame.origin.y + lastFrame.size.height)
        }
        drawText(NSLocalizedString("prettyTechSkills", comment: ""), frameRect: rect, isTitle: true)
        
        let textString = createSkillsContentString(skill)
        
        templateView.labelTechContent.text = textString
        templateView.labelTechContent.sizeToFit()
        if checkSize(rect.origin.y + rect.size.height, heightContent: templateView.labelTechContent.frame.height) {
            templateView.labelTechContent.frame.origin.y = (rect.origin.y + rect.size.height) - self.diff
            self.diff = 0
        } else {
            templateView.labelTechContent.frame.origin.y = (rect.origin.y + rect.size.height)
        }
        drawText(textString, frameRect: templateView.labelTechContent.frame, isTitle: false)
    }
    
    func drawAdditionalInfo() {
        let info = (self.resume?.additionalInfo)!
        
        var rect = templateView.labelAdditionalInfo.frame
        let lastFrame = templateView.labelTechContent.frame
        if checkSize(lastFrame.origin.y + lastFrame.size.height, heightContent: rect.height) {
            rect.origin.y = (lastFrame.origin.y + lastFrame.size.height) - self.diff
            self.diff = 0
        } else {
            rect.origin.y = (lastFrame.origin.y + lastFrame.size.height)
        }
        drawText(NSLocalizedString("additionalInfo", comment: ""), frameRect: rect, isTitle: true)
        
        let textString = createAdditionalInfoContentString(info)
        
        templateView.labelAdditionalInfoContent.text = textString
        templateView.labelAdditionalInfoContent.sizeToFit()
        if checkSize((rect.origin.y + rect.size.height), heightContent: templateView.labelAdditionalInfoContent.frame.height) {
            templateView.labelAdditionalInfoContent.frame.origin.y = (rect.origin.y + rect.size.height) - self.diff
            self.diff = 0
        } else {
            templateView.labelAdditionalInfoContent.frame.origin.y = (rect.origin.y + rect.size.height)
        }
        
        drawText(textString, frameRect: templateView.labelAdditionalInfoContent.frame, isTitle: false)
    }
    
    func drawText(textToDraw:String, frameRect:CGRect, isTitle:Bool, isName:Bool=false) {
        
        let length=textToDraw.characters.count
        let string = textToDraw as CFStringRef
        let currentText = CFAttributedStringCreateMutable(kCFAllocatorDefault, 0)
        CFAttributedStringReplaceString (currentText,CFRangeMake(0, 0), string)
        if isTitle {
            CFAttributedStringSetAttribute(currentText,CFRangeMake(0, length),kCTFontAttributeName,self.fontTitle)
            if !isName {
                let newFrame = CGRectMake(frameRect.origin.x, frameRect.origin.y + 32, frameRect.size.width, 1)
                addLineWithFrame(newFrame)
                //CFAttributedStringSetAttribute(currentText,CFRangeMake(0, length),kCTUnderlineStyleAttributeName, NSNumber(int: CTUnderlineStyle.Single.rawValue))
                
            }
        } else {
            CFAttributedStringSetAttribute(currentText,CFRangeMake(0, length),kCTFontAttributeName,self.fontNormal)
        }
        
        let frameSetter = CTFramesetterCreateWithAttributedString(currentText)
        
        let framePath = CGPathCreateMutable()
        CGPathAddRect(framePath, nil, frameRect)
        
        let currentRange = CFRangeMake(0, 0)
        let frameRef = CTFramesetterCreateFrame(frameSetter, currentRange, framePath, nil)
        
        let currentContext = UIGraphicsGetCurrentContext()
        
        CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity)
        CGContextTranslateCTM(currentContext, 0, frameRect.origin.y*2 + frameRect.size.height)
        CGContextScaleCTM(currentContext, 1.0, -1.0)
        
        // Draw the frame.
        CTFrameDraw(frameRef, currentContext!)
        
        CGContextScaleCTM(currentContext, 1.0, -1.0)
        CGContextTranslateCTM(currentContext, 0, (-1)*frameRect.origin.y*2 - frameRect.size.height)
        
    }
    
    func addLineWithFrame(frame:CGRect) {
        let currentContext = UIGraphicsGetCurrentContext()
    
        CGContextSetStrokeColorWithColor(currentContext, UIColor.blackColor().CGColor);
    
        // this is the thickness of the line
        CGContextSetLineWidth(currentContext, frame.size.height);
    
        let startPoint = frame.origin;
        let endPoint = CGPointMake(frame.origin.x + frame.size.width, frame.origin.y);
    
        CGContextBeginPath(currentContext);
        CGContextMoveToPoint(currentContext, startPoint.x, startPoint.y);
        CGContextAddLineToPoint(currentContext, endPoint.x, endPoint.y);
    
        CGContextDrawPath(currentContext, .FillStroke);
    
    }

    
    func drawTextWithBoldTitles(textToDraw:String, frameRect:CGRect, ranges:[CFRange]) {
        
        let length=textToDraw.characters.count
        let string = textToDraw as CFStringRef
        let currentText = CFAttributedStringCreateMutable(kCFAllocatorDefault, 0)
        CFAttributedStringReplaceString (currentText,CFRangeMake(0, 0), string)
        CFAttributedStringSetAttribute(currentText,CFRangeMake(0, length),kCTFontAttributeName,self.fontNormal)
        
        for range in ranges {
            CFAttributedStringSetAttribute(currentText,range,kCTFontAttributeName,self.fontNormalBold)
        }
        
        let frameSetter = CTFramesetterCreateWithAttributedString(currentText)
        
        let framePath = CGPathCreateMutable()
        CGPathAddRect(framePath, nil, frameRect)
        
        let currentRange = CFRangeMake(0, 0)
        let frameRef = CTFramesetterCreateFrame(frameSetter, currentRange, framePath, nil)
        
        let currentContext = UIGraphicsGetCurrentContext()
        
        CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity)
        CGContextTranslateCTM(currentContext, 0, frameRect.origin.y*2 + frameRect.size.height)
        CGContextScaleCTM(currentContext, 1.0, -1.0)
        
        // Draw the frame.
        CTFrameDraw(frameRef, currentContext!)
        
        CGContextScaleCTM(currentContext, 1.0, -1.0)
        CGContextTranslateCTM(currentContext, 0, (-1)*frameRect.origin.y*2 - frameRect.size.height)
        
    }

    func createHeaderString() -> String {
        let personal = (self.resume!.personal)!
        var header = ""
        if personal.address != "" {
            header = "\(personal.address!), \(personal.state!), \(personal.city!)"
        } else {
            header = "\(personal.state!), \(personal.city!)"
        }
        
        return header
    }
    
    func createHeaderContactString() -> String {
        let personal = (self.resume!.personal)!
        var header = ""
        if personal.phone2 != "" {
            header = "Email: \(personal.email!), \(NSLocalizedString("phone", comment: "")): \(personal.phone!),\(personal.phone2!)"
        } else {
            header = "Email: \(personal.email!), \(NSLocalizedString("phone", comment: "")): \(personal.phone!)"
        }
        return header
    }
    
    func createEducationContentString(education:NSSet) -> String {
        var content = ""
        let educations = education.allObjects as! [Education]
        for ed in educations {
            let dateFormat = NSDateFormatter()
            dateFormat.dateFormat = "MMM/yyyy"
                
            content.appendContentsOf("\n\(ed.instName!), \(ed.location!)")
            content.appendContentsOf("\n\(ed.major!): \(dateFormat.stringFromDate(ed.from!)) - \(dateFormat.stringFromDate(ed.to!))")
            content.appendContentsOf("\n")
        }
        return content
    }
    
    func getEducationRanges(education:NSSet, text: String) -> [CFRange] {
        var ranges = [CFRange]()
        let educations = education.allObjects as! [Education]
        
        for ed in educations {
            let index = text.rangeOfString(ed.instName!)
            let start = text.startIndex.distanceTo((index?.startIndex)!)
            ranges.append( CFRange(location: start, length: ed.instName!.characters.count))
        }
        return ranges
    }
    
    func createExperienceContentString(exps:NSSet) -> String {
        var content = ""
        let experiences = exps.allObjects as! [Experience]
        for exp in experiences {
            let dateFormat = NSDateFormatter()
            dateFormat.dateFormat = "MMM/yyyy"
            
            content.appendContentsOf("\n\(exp.orgName!): \(dateFormat.stringFromDate(exp.from!)) - \(dateFormat.stringFromDate(exp.to!))")
            content.appendContentsOf("\n\(exp.role!) - \(exp.jobDesc!)")
            content.appendContentsOf("\n")
            
        }
        return content
    }
    
    func getExperienceRanges(experiences:NSSet, text: String) -> [CFRange] {
        var ranges = [CFRange]()
        let exps = experiences.allObjects as! [Experience]
        
        for exp in exps {
            let index = text.rangeOfString(exp.orgName!)
            let start = text.startIndex.distanceTo((index?.startIndex)!)
            ranges.append( CFRange(location: start, length: exp.orgName!.characters.count))
        }
        return ranges
    }

    func createSkillsContentString(skill:Skills) -> String {
        var content = ""
        content.appendContentsOf("\n")
        content.appendContentsOf("\(skill.skillsDesc!)")
        content.appendContentsOf("\n")
        return content;
    }
    
    func createAdditionalInfoContentString(info:AdditionalInfo) -> String {
        var content = ""
        content.appendContentsOf("\n")
        content.appendContentsOf("\(info.information!)")
        content.appendContentsOf("\n")
        return content;
    }
    
    func checkSize(nextY:CGFloat, heightContent:CGFloat) -> Bool{
        if (nextY + heightContent) - self.diff > 792 {
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil)
            self.diff = nextY
            return true
        }
        return false
    }
}

//
//  AboutViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/20/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit
import MessageUI

class AboutViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("about", comment: "")
        
        let aboutView = AboutView()
        aboutView.buttonContact.addTarget(self, action: #selector(self.contactButtonTapped(_:)), forControlEvents: .TouchUpInside)
        aboutView.buttonInstructions.addTarget(self, action: #selector(self.showInstructions(_:)), forControlEvents: .TouchUpInside)
        aboutView.buttonRate.addTarget(self, action: #selector(self.rateApp(_:)), forControlEvents: .TouchUpInside)
        
        let scroll = UIScrollView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        scroll.contentSize = CGSize(width: aboutView.frame.width, height: aboutView.frame.height)
        scroll.delaysContentTouches = false
        scroll.addSubview(aboutView)
        
        self.view.addSubview(scroll)
        
    }
    
    func contactButtonTapped(sender: AnyObject) {
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Curriculum Manager IOS")
        mailComposer.setToRecipients(["wcaf0702@gmail.com"])
        
        self.presentViewController(mailComposer, animated: true, completion: nil)
    }
    
    func showInstructions(sender: AnyObject) {
        performSegueWithIdentifier("showInstructions", sender: "")
    }
    
    func rateApp(sender: AnyObject){
        UIApplication.sharedApplication().openURL(NSURL(string : "itms-apps://itunes.apple.com/app/id1099974785")!);

    }
    
    //MARK: MailComposerDelete
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

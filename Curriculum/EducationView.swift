//
//  EducationView.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/24/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class EducationView: UIView {
    
    @IBOutlet weak var labelInst: UILabel!
    @IBOutlet weak var fieldInst: UITextField!
    @IBOutlet weak var labelMajor: UILabel!
    @IBOutlet weak var fieldMajor: UITextField!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var fieldLocation: UITextField!
    @IBOutlet weak var labelFrom: UILabel!
    @IBOutlet weak var dateFrom: UIDatePicker!
    @IBOutlet weak var labelTo: UILabel!
    @IBOutlet weak var dateTo: UIDatePicker!
    
    init() {
        super.init(frame: CGRectMake(0, 0, 0, 0))
        setup()
        adaptLanguage()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        let xib = NSBundle.mainBundle().loadNibNamed("EducationView", owner: self, options: nil)[0] as! EducationView
        
        xib.layer.borderWidth = 1
        xib.layer.cornerRadius = 8
        
        xib.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width - 10, 600)
        self.frame = xib.frame
        self.addSubview(xib)
    }
    
    func adaptLanguage() {
        self.labelInst.text = "\(NSLocalizedString("instName", comment: "")):"
        self.fieldInst.placeholder = "\(NSLocalizedString("instPlaceH", comment: ""))"
        self.labelMajor.text = "\(NSLocalizedString("major", comment: "")):"
        self.fieldMajor.placeholder = "\(NSLocalizedString("majorPlaceH", comment: ""))"
        self.labelLocation.text = "\(NSLocalizedString("location", comment: "")):"
        self.fieldLocation.placeholder = NSLocalizedString("locationPlaceH", comment: "")
        self.labelFrom.text = "\(NSLocalizedString("from", comment: "")):"
        self.labelTo.text = "\(NSLocalizedString("to", comment: "")):"
        
    }
    
    func saveFields() -> Education? {
        if self.fieldInst.text != "" && self.fieldMajor.text != "" {
            let education = Education(nameInst: self.fieldInst.text!, major: self.fieldMajor.text!, dataFrom: self.dateFrom.date, dataTo: self.dateTo.date)
            education.location = self.fieldLocation.text
            return education
        } else {
            return nil
        }
    }
    
    func loadData(ed: Education) {
        self.fieldInst.text = ed.instName
        self.fieldMajor.text = ed.major
        self.fieldLocation.text = ed.location
        self.dateFrom.setDate(ed.from!, animated: true)
        self.dateTo.setDate(ed.to!, animated: true)
    }
}

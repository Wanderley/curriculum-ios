//
//  Personal.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/22/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit
import CoreData

class PersonalView: UIView {

    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var fieldName: UITextField!
    @IBOutlet weak var labelAge: UILabel!
    @IBOutlet weak var fieldAge: UITextField!
    @IBOutlet weak var labelCountry: UILabel!
    @IBOutlet weak var fieldCountry: UITextField!
    @IBOutlet weak var labelState: UILabel!
    @IBOutlet weak var fieldState: UITextField!
    @IBOutlet weak var labelCity: UILabel!
    @IBOutlet weak var fieldCity: UITextField!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var fieldAddress: UITextField!
    @IBOutlet weak var labelZip: UILabel!
    @IBOutlet weak var fieldZip: UITextField!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var fieldPhone: UITextField!
    @IBOutlet weak var labelPhone2: UILabel!
    @IBOutlet weak var fieldPhone2: UITextField!
    @IBOutlet weak var fieldEmail: UITextField!
    @IBOutlet weak var labelLinkedin: UILabel!
    @IBOutlet weak var fieldLinkedin: UITextField!
    @IBOutlet weak var labelFacebook: UILabel!
    @IBOutlet weak var fieldFacebook: UITextField!
    
    var wasImageChanged: Bool = false
    
    init() {
        super.init(frame: CGRectMake(0, 0, 0, 0))
        setup()
        adaptLanguage()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
      
    }
    //MARK: setupView
    func setup() {
        let xib = NSBundle.mainBundle().loadNibNamed("PersonalView", owner: self, options: nil)[0] as! PersonalView
        xib.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 1110)
        self.frame = xib.frame
        self.addSubview(xib)
        self.imageProfile.layer.cornerRadius = 10
    }
    
    func adaptLanguage() {
        self.labelName.text = "\(NSLocalizedString("labelName", comment: "")):"
        self.labelAge.text = "\(NSLocalizedString("labelAge", comment: "")):"
        self.labelCountry.text = "\(NSLocalizedString("labelCountry", comment: "")):"
        self.labelState.text = "\(NSLocalizedString("labelState", comment: "")):"
        self.labelCity.text = "\(NSLocalizedString("labelCity", comment: "")):"
        self.labelAddress.text = "\(NSLocalizedString("labelAddress", comment: "")):"
        self.labelZip.text = "\(NSLocalizedString("labelZip", comment: "")):"
        self.labelPhone.text = "\(NSLocalizedString("labelPhone", comment: "")):"
        self.labelPhone2.text = "\(NSLocalizedString("labelPhone2", comment: "")):"
    }
    
    //MARK: validation and Data
    
    func validateAllFields() -> Bool {
        if self.fieldName.text != "" && self.fieldAge.text != "" && self.fieldCountry.text != "" && self.fieldState.text != "" && self.fieldCity.text != "" && self.fieldEmail.text != "" && self.fieldPhone.text != "" {
            return true
        } else {
            return false
        }
    }
    
    func saveFields() -> Personal {
        let personal = Personal(name: self.fieldName.text!, age: self.fieldAge.text!, country: self.fieldCountry.text!, state: self.fieldState.text!, city: self.fieldCity.text!, phone: self.fieldPhone.text!)
        personal.address = self.fieldAddress.text
        personal.phone2 = self.fieldPhone2.text
        personal.zip = self.fieldZip.text
        personal.linkedin = self.fieldLinkedin.text
        personal.facebook = self.fieldFacebook.text
        personal.email = self.fieldEmail.text
        
        return personal
    }
    
    func loadData(resume:Resume) {
        let personal = resume.personal!
        self.fieldName.text = personal.name
        self.fieldAge.text = personal.age
        self.fieldCountry.text = personal.country
        self.fieldState.text = personal.state
        self.fieldCity.text = personal.city
        self.fieldPhone.text = personal.phone
        if let imgData = resume.image?.image {
            self.imageProfile.image = UIImage(data: imgData)
        }
        self.fieldAddress.text = personal.address
        self.fieldPhone2.text = personal.phone2
        self.fieldZip.text = personal.zip
        self.fieldLinkedin.text = personal.linkedin
        self.fieldFacebook.text = personal.facebook
        self.fieldEmail.text = personal.email
    }
    
    func getChosenImage() -> NSData? {
        if self.wasImageChanged {
            let imgData = UIImagePNGRepresentation(self.imageProfile.image!)
            return imgData
        }
        return nil
    }
}

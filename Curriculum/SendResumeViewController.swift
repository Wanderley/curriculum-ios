//
//  SendResumeViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/20/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class SendResumeViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    var resumes:[Resume]?
    let loadQueue = dispatch_queue_create("loadImages", DISPATCH_QUEUE_CONCURRENT)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("sendResume", comment: "")
        tableView.registerNib(UINib(nibName: "CurriculumCell", bundle: nil), forCellReuseIdentifier: "CurriculumCell")

    }
    
    //MARK: initial setup
    override func viewWillAppear(animated: Bool) {
        self.loadResumes()
        self.tableView.reloadData()
    }
    
    func loadResumes() {
        do {
            self.resumes = try DataController.moc.executeFetchRequest(NSFetchRequest(entityName: "Resume")) as! [Resume]
        } catch {
            fatalError("Message error :\(error)")
        }
    }


    //MARK: tableView
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.resumes?.count)!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CurriculumCell", forIndexPath: indexPath) as! CurriculumTableViewCell
        let resume = self.resumes![indexPath.row]
        cell.labelName.text = resume.owner?.stringByReplacingOccurrencesOfString("_", withString: " ")
        cell.imageResume.layer.cornerRadius = 3
        
        if resume.owner == NSLocalizedString("notDefined", comment: "") {
            cell.labelDetails.text = NSLocalizedString("notDefined", comment: "")
        } else {
            self.loadImageInBackGround(resume.image, imagePosition: cell.imageResume)
            
            let age = resume.personal?.age
            let email = resume.personal?.email
            cell.labelDetails.text = "\(age!), \(email!)"
            
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let resume = self.resumes![indexPath.row]
        
        let response = PDFManager.checkIfPdfExists(resume)
        
        if !response {
            let alert = UIAlertController(title: NSLocalizedString("alert", comment: "alert"), message: NSLocalizedString("visualizeFirstEmail", comment: "notValidated"), preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
            sendEmail(resume)
        }
        
    }
    
    func sendEmail(resume:Resume) {
        let path = PDFManager.getPDFPath(resume)
        
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        let fileData = NSData(contentsOfFile: path)
        
        mailComposer.addAttachmentData(fileData!, mimeType: "application/pdf", fileName: "\(resume.lastPdfName!).pdf")
        do {
            try self.presentViewController(mailComposer, animated: true, completion: nil)
        } catch {
            fatalError("Erro ao tentar enviar email \(error)")
        }
    }
    
    //MARK: MailComposerDelete
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: concurrent tasks
    func loadImageInBackGround(imageResume:ImageResume?, imagePosition: UIImageView) {
        
        dispatch_async(self.loadQueue) {
            if let data = imageResume?.image {
                let image = UIImage(data: data)
                dispatch_async(dispatch_get_main_queue()) {
                    imagePosition.image = image
                }
            }
        }
    }

}

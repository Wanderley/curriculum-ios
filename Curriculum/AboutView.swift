//
//  AboutView.swift
//  Curriculum
//
//  Created by Wanderley Filho on 4/2/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit
import MessageUI

class AboutView: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelContact: UILabel!
    @IBOutlet weak var buttonRate: UIButton!
    @IBOutlet weak var buttonInstructions: UIButton!
    @IBOutlet weak var buttonContact: UIButton!
    
    init() {
        super.init(frame: CGRectZero)
        setup()
        adaptLanguage()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        let xib = NSBundle.mainBundle().loadNibNamed("AboutView", owner: self, options: nil)[0] as! AboutView
        xib.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, xib.frame.height)
        self.frame = xib.frame
        self.addSubview(xib)
    }
    
    func adaptLanguage() {
        self.labelTitle.text = NSLocalizedString("aboutMainMessage", comment: "")
        self.labelDescription.text = NSLocalizedString("aboutDescMessage", comment: "")
        self.labelContact.text = NSLocalizedString("aboutContactMessage", comment: "")
        self.buttonRate.setTitle(NSLocalizedString("rate", comment: ""), forState: .Normal)
        self.buttonInstructions.setTitle(NSLocalizedString("instructions", comment: ""), forState: .Normal)
        self.buttonContact.setTitle(NSLocalizedString("contact", comment: ""), forState: .Normal)
    }
    
}

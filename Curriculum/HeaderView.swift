//
//  headerView.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/31/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class HeaderView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var viewCompleted: UIView!
    @IBOutlet weak var viewIncompleted: UIView!
    @IBOutlet weak var viewEmpty: UIView!
    @IBOutlet weak var labelCompleted: UILabel!
    @IBOutlet weak var labelIncompleted: UILabel!
    @IBOutlet weak var labelIEmpty: UILabel!

    init() {
        super.init(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 40))
        setup()
        roundViews()
        setupLabels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        let xib = NSBundle.mainBundle().loadNibNamed("HeaderView", owner: self, options: nil)[0] as! HeaderView
        xib.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 40)
        self.frame = xib.frame
        self.addSubview(xib)
    }
    
    func roundViews() {
        self.viewCompleted.layer.borderWidth = 0.5
        self.viewCompleted.layer.cornerRadius = 13
        
        self.viewIncompleted.layer.borderWidth = 0.5
        self.viewIncompleted.layer.cornerRadius = 13
        
        self.viewEmpty.layer.borderWidth = 0.5
        self.viewEmpty.layer.cornerRadius = 13
    }
    
    func setupLabels() {
        self.labelCompleted.text = NSLocalizedString("completed", comment: "")
        self.labelIncompleted.text = NSLocalizedString("incompleted", comment: "")
        self.labelIEmpty.text =  NSLocalizedString("empty", comment: "")
    }
}

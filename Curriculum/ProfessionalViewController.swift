//
//  ProfessionalViewController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class ProfessionalViewController: UIViewController {

    var resume: Resume?
    var professionalViews = [ProfessionalView]()
    var addBtn:UIButton!
    var scrollView: UIScrollView!
    var limitCounter = 4;
    
    //QUEUES
    let loadExpsQueue = dispatch_queue_create("loadExps", DISPATCH_QUEUE_CONCURRENT)
    let saveExpsQueue = dispatch_queue_create("saveExps", DISPATCH_QUEUE_CONCURRENT)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("professionalExp", comment: "")
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfessionalViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        let ed = ProfessionalView()
        ed.frame = CGRectMake(5, getBottomPosition(), UIScreen.mainScreen().bounds.width - 10, 700)
        self.professionalViews.append(ed)
        self.scrollView = UIScrollView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        self.scrollView.contentSize = CGSize(width: UIScreen.mainScreen().bounds.width, height: professionalViews[0].frame.height + 60)
        
        self.scrollView.addSubview(professionalViews[0])
        
        self.addBtn = UIButton(type: UIButtonType.System) as UIButton
        self.addBtn.frame = CGRectMake(10,getBottomPosition(),UIScreen.mainScreen().bounds.width-20,40)
        self.addBtn.titleLabel?.font = UIFont(name: (self.addBtn.titleLabel?.font?.fontName)!, size: 16)
        self.addBtn.setTitle("\(NSLocalizedString("addButton", comment: "")) \(NSLocalizedString("professionalExp", comment: ""))", forState: .Normal)
        self.addBtn.addTarget(self, action: #selector(ProfessionalViewController.addNewExperience), forControlEvents: .TouchUpInside)
        
        self.loadExperienceData()
        
        scrollView.addSubview(self.addBtn)
        self.view.addSubview(scrollView)

    }
    
    //MARK: managing view
    
    func addNewExperience() {
        let pr = ProfessionalView()
        pr.frame = CGRectMake(5, getBottomPosition(), UIScreen.mainScreen().bounds.width - 10, 700)
        self.professionalViews.append(pr)
        self.scrollView.contentSize.height = CGFloat((self.professionalViews.count * 705) + 60)
        self.scrollView.addSubview(pr)
        self.addBtn.frame.origin.y = self.getBottomPosition()
        self.limitCounter -= 1
        
        if self.limitCounter == 0 {
            self.addBtn.removeFromSuperview()
        }
    }

    func getBottomPosition() -> CGFloat{
        var posY:CGFloat = 5
        for _ in self.professionalViews {
            posY += 705
        }
        return posY
    }

    //MARK: keyboard handling
    
    func keyboardWillShow(notification: NSNotification) {
        for exp in professionalViews {
            exp.dateFrom.userInteractionEnabled = false;
            exp.dateTo.userInteractionEnabled = false;
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        for exp in professionalViews {
            exp.dateFrom.userInteractionEnabled = true;
            exp.dateTo.userInteractionEnabled = true;
        }
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        self.saveExperiencesData()
        let dest = segue.destinationViewController as! TechSkillViewController
        dest.resume = self.resume
    }

    //MARK: Concurrent Tasks
    func loadExperienceData() {
        let exps = self.resume?.experience?.allObjects as! [Experience]
        let count = exps.count
        if count > 0 {
            for _ in 0 ..< (count - 1) {
                self.addNewExperience()
            }
            dispatch_async(self.loadExpsQueue) {
                for index in 0 ..< count {
                    let expView = self.professionalViews[index]
                    let expData = exps[index]
                    dispatch_async(dispatch_get_main_queue()) {
                        expView.loadData(expData)
                    }
                }
            }
        }
    }
    
    func saveExperiencesData() {
        dispatch_async(self.saveExpsQueue) {
            let exps = NSMutableSet()
            for expView in self.professionalViews {
                if let exp = expView.saveFields() {
                    exps.addObject(exp)
                }
            }
            self.resume?.experience = exps
            do {
                try DataController.moc.save()
            } catch {
                fatalError("Error during saving educations: \(error)")
            }
        }
    }
}

//
//  TabBarController.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/20/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    @IBOutlet weak var tabs: UITabBar!
    let titles = [NSLocalizedString("resumes", comment: "resumes"),NSLocalizedString("viewPdf", comment: "viewPdf"),NSLocalizedString("sendResume", comment: "sendResume"),NSLocalizedString("about", comment: "about"),]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool) {
        //Updating the titles of each tab
        for i in  0..<4 {
            self.tabs.items![i].title = self.titles[i]
        }
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

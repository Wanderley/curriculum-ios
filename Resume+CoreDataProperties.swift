//
//  Resume+CoreDataProperties.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/31/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Resume {

    @NSManaged var lastPdfName: String?
    @NSManaged var owner: String?
    @NSManaged var additionalInfo: AdditionalInfo?
    @NSManaged var education: NSSet?
    @NSManaged var experience: NSSet?
    @NSManaged var personal: Personal?
    @NSManaged var skills: Skills?
    @NSManaged var image: ImageResume?

}

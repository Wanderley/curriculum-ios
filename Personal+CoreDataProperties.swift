//
//  Personal+CoreDataProperties.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/31/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Personal {

    @NSManaged var address: String?
    @NSManaged var age: String?
    @NSManaged var city: String?
    @NSManaged var country: String?
    @NSManaged var email: String?
    @NSManaged var facebook: String?
    @NSManaged var linkedin: String?
    @NSManaged var name: String?
    @NSManaged var phone: String?
    @NSManaged var phone2: String?
    @NSManaged var state: String?
    @NSManaged var zip: String?
    @NSManaged var resume: Resume?

}

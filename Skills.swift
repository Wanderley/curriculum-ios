//
//  Skills.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import Foundation
import CoreData


class Skills: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    convenience init(skills:String) {
        self.init(entity: NSEntityDescription.entityForName("Skills", inManagedObjectContext: DataController.moc)!, insertIntoManagedObjectContext: DataController.moc)
        self.skillsDesc = skills
    }
}

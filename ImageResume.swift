//
//  ImageResume.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/31/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import Foundation
import CoreData


class ImageResume: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    convenience init() {
        self.init(entity: NSEntityDescription.entityForName("ImageResume", inManagedObjectContext: DataController.moc)!, insertIntoManagedObjectContext: DataController.moc)
    }
}

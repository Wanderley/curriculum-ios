//
//  Experience.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import Foundation
import CoreData


class Experience: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    convenience init(organization: String, role:String, isCurrent:Bool, from:NSDate) {
        self.init(entity: NSEntityDescription.entityForName("Experience", inManagedObjectContext: DataController.moc)!, insertIntoManagedObjectContext: DataController.moc)
        self.orgName = organization
        self.role = role
        self.isCurrent = isCurrent
        self.from = from
    }

}

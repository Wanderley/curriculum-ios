//
//  Education+CoreDataProperties.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/25/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Education {

    @NSManaged var instName: String?
    @NSManaged var major: String?
    @NSManaged var location: String?
    @NSManaged var from: NSDate?
    @NSManaged var to: NSDate?
    @NSManaged var resume: Resume?

}

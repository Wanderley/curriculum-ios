//
//  ImageResume+CoreDataProperties.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/31/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ImageResume {

    @NSManaged var image: NSData?
    @NSManaged var resume: Resume?

}

//
//  Personal.swift
//  Curriculum
//
//  Created by Wanderley Filho on 3/31/16.
//  Copyright © 2016 Wanderley Filho. All rights reserved.
//

import Foundation
import CoreData


class Personal: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    convenience init(name: String, age: String, country: String, state: String, city: String, phone: String) {
        
        self.init(entity: NSEntityDescription.entityForName("Personal", inManagedObjectContext: DataController.moc)!, insertIntoManagedObjectContext: DataController.moc)
        self.name = name
        self.age = age
        self.country = country
        self.state = state
        self.city = city
        self.phone = phone
    }

}
